package com.dpelluzi.githubexample;

import com.dpelluzi.githubexample.interactors.RepositoryInteractor;
import com.dpelluzi.githubexample.interfaces.RepositoryListContract;
import com.dpelluzi.githubexample.models.RepoSearchResult;
import com.dpelluzi.githubexample.presenters.RepositoryListPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryListPresenterTest {

    @Mock
    private RepositoryListContract.View mView;

    @Mock
    private RepositoryInteractor mRepository;

    @Captor
    private ArgumentCaptor<RepositoryInteractor.RepoSearchCallback> mCallbackCaptor;

    private RepositoryListContract.Presenter mPresenter;

    private RepoSearchResult searchResult;

    @Before
    public void setup() {
        mPresenter = new RepositoryListPresenter(mView, mRepository);
        searchResult = new RepoSearchResult();
    }

    @Test
    public void testSearchRepo_onSuccess() {
        mPresenter.onViewCreated();

        verify(mView).setupViews();
        verify(mView).showProgressBar();

        verify(mRepository).searchJavaRepositories(mCallbackCaptor.capture(), eq(1));
        mCallbackCaptor.getValue().onResult(searchResult);

        verify(mView).dismissProgressBar();
        verify(mView).showList();
        verify(mView).addRepositories(eq(searchResult.items));
    }

    @Test
    public void testSearchRepo_onError() {
        mPresenter.onViewCreated();

        verify(mView).setupViews();
        verify(mView).showProgressBar();

        verify(mRepository).searchJavaRepositories(mCallbackCaptor.capture(), eq(1));
        mCallbackCaptor.getValue().onError();

        verify(mView).dismissProgressBar();
        verify(mView).showError();
    }

}
