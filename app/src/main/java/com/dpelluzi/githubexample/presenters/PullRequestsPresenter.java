package com.dpelluzi.githubexample.presenters;

import com.dpelluzi.githubexample.interactors.PullRequestInteractor;
import com.dpelluzi.githubexample.interfaces.PullRequestListContract;
import com.dpelluzi.githubexample.models.GitHubRepo;
import com.dpelluzi.githubexample.models.PullRequest;

import java.util.List;

public class PullRequestsPresenter implements PullRequestListContract.Presenter {

    private PullRequestListContract.View mView;
    private PullRequestInteractor mInteractor;
    private int mNextPage = 1;
    private boolean mHasMorePages;
    private GitHubRepo mRepo;

    public PullRequestsPresenter(PullRequestListContract.View view, PullRequestInteractor repositoryInteractor, GitHubRepo repo) {
        mView = view;
        mInteractor = repositoryInteractor;
        mRepo = repo;
    }

    @Override
    public void onViewCreated() {
        mView.setupViews();
        mView.showProgressBar();

        getPullRequests();
    }

    @Override
    public void onPullRequestClicked(PullRequest pullRequest) {
        mView.openRepoUrl(pullRequest.url);
    }

    @Override
    public void loadMoreData() {
        if (mHasMorePages) {
            getPullRequests();
        }
    }

    private void getPullRequests() {
        mInteractor.getPullRequests(new PullRequestInteractor.GetPullRequestCallback() {
            @Override
            public void onResult(List<PullRequest> result) {
                mHasMorePages = result.size() > mView.getItemCount();
                mNextPage++;

                mView.dismissProgressBar();
                mView.showList();
                mView.addPullRequests(result);
            }
            @Override
            public void onError() {
                mView.dismissProgressBar();
                mView.showError();
            }
        }, mRepo.owner.username, mRepo.name, mNextPage);
    }
}
