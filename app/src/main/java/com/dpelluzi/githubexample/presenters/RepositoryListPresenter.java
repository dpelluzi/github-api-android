package com.dpelluzi.githubexample.presenters;

import com.dpelluzi.githubexample.interactors.RepositoryInteractor;
import com.dpelluzi.githubexample.interfaces.RepositoryListContract;
import com.dpelluzi.githubexample.models.GitHubRepo;
import com.dpelluzi.githubexample.models.RepoSearchResult;

public class RepositoryListPresenter implements RepositoryListContract.Presenter {

    private RepositoryListContract.View mView;
    private RepositoryInteractor mInteractor;
    private int mNextPage = 1;
    private boolean mHasMorePages;

    public RepositoryListPresenter(RepositoryListContract.View view, RepositoryInteractor repositoryInteractor) {
        mView = view;
        mInteractor = repositoryInteractor;
    }

    @Override
    public void onViewCreated() {
        mView.setupViews();
        mView.showProgressBar();

        getRepositories();
    }

    @Override
    public void onRepositoryClicked(GitHubRepo repo) {
        mView.startPullRequest(repo);
    }

    @Override
    public void loadMoreData() {
        if (mHasMorePages) {
            getRepositories();
        }
    }

    private void getRepositories() {
        mInteractor.searchJavaRepositories(new RepositoryInteractor.RepoSearchCallback() {
            @Override
            public void onResult(RepoSearchResult result) {
                mHasMorePages = result.totalCount > mView.getItemCount();
                mNextPage++;

                mView.dismissProgressBar();
                mView.showList();
                mView.addRepositories(result.items);
            }
            @Override
            public void onError() {
                mView.dismissProgressBar();
                mView.showError();
            }
        }, mNextPage);
    }
}
