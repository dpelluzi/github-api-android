package com.dpelluzi.githubexample.ui;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dpelluzi.githubexample.R;
import com.dpelluzi.githubexample.interfaces.BaseListContract;
import com.dpelluzi.githubexample.ui.adapters.BaseListAdapter;

import butterknife.BindView;

public abstract class BaseListActivity extends AppCompatActivity implements BaseListContract.BaseView {

    public static final String EXTRA_REPO = "extra.REPO";

    @BindView(R.id.list)
    RecyclerView mMovieList;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    protected BaseListAdapter mAdapter;

    @Override
    public abstract void setupViews();

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showList() {
        mMovieList.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mAdapter.getItemCount();
    }
}
