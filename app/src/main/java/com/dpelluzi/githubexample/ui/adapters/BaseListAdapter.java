package com.dpelluzi.githubexample.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public abstract class BaseListAdapter<T> extends RecyclerView.Adapter<BaseListAdapter.BaseViewHolder> {

    public interface OnItemClickListener<T> {

        void onItemClicked(T item);
    }

    private List<T> mData;
    private OnItemClickListener mOnItemClickListener;

    public BaseListAdapter() {
        mData = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void addItems(List<T> items) {
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public abstract class BaseViewHolder<R> extends RecyclerView.ViewHolder implements View.OnClickListener {

        public BaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public abstract void bind(@NonNull R item);

        @Override
        public void onClick(View view) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClicked(mData.get(getAdapterPosition()));
            }
        }
    }
}
