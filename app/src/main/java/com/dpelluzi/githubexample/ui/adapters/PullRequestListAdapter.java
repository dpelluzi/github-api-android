package com.dpelluzi.githubexample.ui.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpelluzi.githubexample.R;
import com.dpelluzi.githubexample.models.PullRequest;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class PullRequestListAdapter extends BaseListAdapter<PullRequest> {

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pull_request, parent, false);
        return new PullRequestViewHolder(view);
    }

    public class PullRequestViewHolder extends BaseViewHolder<PullRequest> {

        @BindView(R.id.text_name)
        TextView nameView;

        @BindView(R.id.text_description)
        TextView descriptionView;

        @BindView(R.id.text_author_name)
        TextView authorNameView;

        @BindView(R.id.img_author)
        ImageView authorImageView;

        public PullRequestViewHolder(View itemView) {
            super(itemView);
        }

        public void bind(@NonNull PullRequest item) {
            nameView.setText(item.title);
            descriptionView.setText(item.description);
            authorNameView.setText(item.user.username);

            Picasso.with(authorImageView.getContext()).load(item.user.avatarUrl).into(authorImageView);
        }
    }
}
