package com.dpelluzi.githubexample.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dpelluzi.githubexample.R;
import com.dpelluzi.githubexample.interactors.RepositoryInteractor;
import com.dpelluzi.githubexample.interfaces.RepositoryListContract;
import com.dpelluzi.githubexample.models.GitHubRepo;
import com.dpelluzi.githubexample.presenters.RepositoryListPresenter;
import com.dpelluzi.githubexample.ui.adapters.BaseListAdapter;
import com.dpelluzi.githubexample.ui.adapters.RepositoryListAdapter;

import java.util.List;

import butterknife.ButterKnife;

public class JavaPopActivity extends BaseListActivity implements RepositoryListContract.View {

    private RepositoryListContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_pop);
        ButterKnife.bind(this);

        mPresenter = new RepositoryListPresenter(this, new RepositoryInteractor());
        mPresenter.onViewCreated();
    }

    @Override
    public void setupViews() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mMovieList.setLayoutManager(layoutManager);
        mAdapter = new RepositoryListAdapter();
        mAdapter.setOnItemClickListener(new BaseListAdapter.OnItemClickListener<GitHubRepo>() {
            @Override
            public void onItemClicked(GitHubRepo repo) {
                mPresenter.onRepositoryClicked(repo);
            }
        });
        mMovieList.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {

            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mPresenter.loadMoreData();
            }
        });
        mMovieList.setAdapter(mAdapter);
    }

    @Override
    public void addRepositories(List<GitHubRepo> items) {
        mAdapter.addItems(items);
    }

    @Override
    public void startPullRequest(GitHubRepo repo) {
        final Intent intent = new Intent(this, PullRequestsActivity.class);
        intent.putExtra(EXTRA_REPO, repo);
        startActivity(intent);
    }
}
