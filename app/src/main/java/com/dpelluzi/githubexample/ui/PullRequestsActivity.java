package com.dpelluzi.githubexample.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.dpelluzi.githubexample.R;
import com.dpelluzi.githubexample.interactors.PullRequestInteractor;
import com.dpelluzi.githubexample.interfaces.PullRequestListContract;
import com.dpelluzi.githubexample.models.GitHubRepo;
import com.dpelluzi.githubexample.models.PullRequest;
import com.dpelluzi.githubexample.presenters.PullRequestsPresenter;
import com.dpelluzi.githubexample.ui.adapters.BaseListAdapter;
import com.dpelluzi.githubexample.ui.adapters.PullRequestListAdapter;

import java.util.List;

import butterknife.ButterKnife;

public class PullRequestsActivity extends BaseListActivity implements PullRequestListContract.View {

    private PullRequestListContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        ButterKnife.bind(this);

        final GitHubRepo repo = getIntent().getParcelableExtra(EXTRA_REPO);
        setTitle(repo.name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPresenter = new PullRequestsPresenter(this, new PullRequestInteractor(), repo);
        mPresenter.onViewCreated();
    }

    @Override
    public void addPullRequests(List<PullRequest> items) {
        mAdapter.addItems(items);
    }

    @Override
    public void setupViews() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mMovieList.setLayoutManager(layoutManager);
        mAdapter = new PullRequestListAdapter();
        mAdapter.setOnItemClickListener(new BaseListAdapter.OnItemClickListener<PullRequest>() {
            @Override
            public void onItemClicked(PullRequest pullRequest) {
                mPresenter.onPullRequestClicked(pullRequest);
            }
        });
        mMovieList.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {

            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mPresenter.loadMoreData();
            }
        });
        mMovieList.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openRepoUrl(String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}
