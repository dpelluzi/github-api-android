package com.dpelluzi.githubexample.ui.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpelluzi.githubexample.R;
import com.dpelluzi.githubexample.models.GitHubRepo;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class RepositoryListAdapter extends BaseListAdapter<GitHubRepo> {

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_repository, parent, false);
        return new RepositoryViewHolder(view);
    }

    public class RepositoryViewHolder extends BaseViewHolder<GitHubRepo> {

        @BindView(R.id.text_name)
        TextView nameView;

        @BindView(R.id.text_description)
        TextView descriptionView;

        @BindView(R.id.text_forks)
        TextView forksView;

        @BindView(R.id.text_stars)
        TextView starsView;

        @BindView(R.id.text_author_name)
        TextView authorNameView;

        @BindView(R.id.img_author)
        ImageView authorImageView;

        public RepositoryViewHolder(View itemView) {
            super(itemView);
        }

        public void bind(@NonNull GitHubRepo item) {
            nameView.setText(item.name);
            descriptionView.setText(item.description);
            forksView.setText(String.valueOf(item.forks));
            starsView.setText(String.valueOf(item.stars));
            authorNameView.setText(item.owner.username);

            Picasso.with(authorImageView.getContext()).load(item.owner.avatarUrl).into(authorImageView);
        }
    }
}
