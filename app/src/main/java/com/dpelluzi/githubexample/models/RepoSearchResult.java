package com.dpelluzi.githubexample.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RepoSearchResult {

    @SerializedName("total_count")
    public int totalCount;

    @SerializedName("items")
    public List<GitHubRepo> items;

    public RepoSearchResult() {
        items = new ArrayList<>();
    }
}
