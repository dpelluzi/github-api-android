package com.dpelluzi.githubexample.models;

import com.google.gson.annotations.SerializedName;

public class PullRequest {

    @SerializedName("title")
    public String title;

    @SerializedName("body")
    public String description;

    @SerializedName("user")
    public User user;

    @SerializedName("html_url")
    public String url;
}
