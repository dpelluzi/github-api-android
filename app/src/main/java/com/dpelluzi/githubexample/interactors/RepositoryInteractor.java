package com.dpelluzi.githubexample.interactors;

import com.dpelluzi.githubexample.interfaces.GitHubApi;
import com.dpelluzi.githubexample.models.RepoSearchResult;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepositoryInteractor {

    public interface RepoSearchCallback {

        void onResult(RepoSearchResult result);

        void onError();
    }

    private static final String URL_GITHUB_API = "https://api.github.com";

    private GitHubApi mGitHubApi;

    public RepositoryInteractor() {
        final Retrofit retrofit = new Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).baseUrl(URL_GITHUB_API).build();

        mGitHubApi = retrofit.create(GitHubApi.class);
    }

    public void searchJavaRepositories(final RepoSearchCallback callback, final int page) {
        mGitHubApi.searchRepositories("language:Java", page).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<RepoSearchResult>() {

            @Override
            public void accept(RepoSearchResult result) throws Exception {
                if (result != null) {
                    callback.onResult(result);
                } else {
                    callback.onError();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                callback.onError();
            }
        });
    }

}
