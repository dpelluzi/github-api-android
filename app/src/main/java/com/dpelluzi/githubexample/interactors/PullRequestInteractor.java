package com.dpelluzi.githubexample.interactors;

import com.dpelluzi.githubexample.interfaces.GitHubApi;
import com.dpelluzi.githubexample.models.PullRequest;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class PullRequestInteractor {

    public interface GetPullRequestCallback {

        void onResult(List<PullRequest> result);

        void onError();
    }

    private static final String URL_GITHUB_API = "https://api.github.com";

    private GitHubApi mGitHubApi;

    public PullRequestInteractor() {
        final Retrofit retrofit = new Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).baseUrl(URL_GITHUB_API).build();

        mGitHubApi = retrofit.create(GitHubApi.class);
    }

    public void getPullRequests(final GetPullRequestCallback callback, final String owner, String repo, int page) {
        mGitHubApi.getPullRequests(owner, repo, page).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<PullRequest>>() {

            @Override
            public void accept(List<PullRequest> result) throws Exception {
                if (result != null) {
                    callback.onResult(result);
                } else {
                    callback.onError();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                callback.onError();
            }
        });
    }

}
