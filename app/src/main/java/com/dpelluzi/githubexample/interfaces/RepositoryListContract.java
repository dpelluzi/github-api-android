package com.dpelluzi.githubexample.interfaces;

import com.dpelluzi.githubexample.models.GitHubRepo;

import java.util.List;

public interface RepositoryListContract {

    interface Presenter extends BaseListContract.BasePresenter {

        void onRepositoryClicked(GitHubRepo repo);

    }

    interface View extends BaseListContract.BaseView {

        void addRepositories(List<GitHubRepo> items);

        void startPullRequest(GitHubRepo repo);
    }
}
