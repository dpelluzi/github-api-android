package com.dpelluzi.githubexample.interfaces;

public interface BaseListContract {

    interface BasePresenter {

        void onViewCreated();

        void loadMoreData();
    }

    interface BaseView {

        void setupViews();

        void showProgressBar();

        void dismissProgressBar();

        void showError();

        void showList();

        int getItemCount();
    }
}
