package com.dpelluzi.githubexample.interfaces;

import com.dpelluzi.githubexample.models.PullRequest;
import com.dpelluzi.githubexample.models.RepoSearchResult;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubApi {

    @GET("search/repositories?sort=stars")
    Single<RepoSearchResult> searchRepositories(@Query("q") String query, @Query("page") int page);

    @GET("repos/{owner}/{repo}/pulls")
    Single<List<PullRequest>> getPullRequests(@Path("owner") String owner, @Path("repo") String repo, @Query("page") int page);

}
