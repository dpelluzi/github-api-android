package com.dpelluzi.githubexample.interfaces;

import com.dpelluzi.githubexample.models.PullRequest;

import java.util.List;

public interface PullRequestListContract {

    interface Presenter extends BaseListContract.BasePresenter {

        void onPullRequestClicked(PullRequest pullRequest);

    }

    interface View extends BaseListContract.BaseView {

        void addPullRequests(List<PullRequest> items);

        void openRepoUrl(String url);
    }
}
